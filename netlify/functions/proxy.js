exports.handler = async (event, context) => {
  // Dynamically import the necessary modules
  const fetchModule = await import('node-fetch');
  const fetch = fetchModule.default || fetchModule;
  const iconvModule = await import('iconv-lite');
  const iconv = iconvModule.default || iconvModule;

  const defaultUrl = 'https://www.36rain.com';
  const url = event.queryStringParameters.url ?  event.queryStringParameters.url : defaultUrl;

  
  // Fetch the content as a Buffer
  const response = await fetch(url);
  const buffer = await response.buffer();

  // Ensure you're accessing the decode function correctly
  // Depending on how iconv-lite is structured, you might directly call iconv.decode
  // However, if the above fails, you may need to adjust based on the actual structure of the import
  const content = iconv.decode(buffer, 'big5');

  // Set the correct headers for your response
  const headers = {
    'Content-Type': 'text/html; charset=UTF-8'
  };

  return {
    statusCode: 200,
    headers,
    body: content.toString(),
  };
};
