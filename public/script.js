// Define the function to load content and re-bind event listeners to links
async function loadContent(url = 'https://www.36rain.com') {
    const response = await fetch('https://36rain.netlify.app/.netlify/functions/proxy?url=' + encodeURIComponent(url));
    const data = await response.text();
    const contentDiv = document.getElementById('content');
    contentDiv.innerHTML = data;

    // After setting the new HTML content, find all links and bind click events
    bindLinkEvents();
}
const baseurl = 'https://www.36rain.com/'
// Bind click events to all links in the content
function bindLinkEvents() {
    const links = document.querySelectorAll('#content a');
    links.forEach(link => {
        link.addEventListener('click', function(e) {
            e.preventDefault(); // Prevent the default link behavior
            const href = e.currentTarget.getAttribute('href');
            loadContent(baseurl.concat(href)); // Load the content for the clicked link
        });
    });
}

// Initial content load
loadContent();

